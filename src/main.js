import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
//import VueRouter from 'vue-router'


const app = createApp(App).use(VueAxios, axios)
app.mount('#app')